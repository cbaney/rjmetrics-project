# Metrimart product page

## Dependencies
+ Node.js

## Install using NPM

```bash
$ npm install
```

## Process javascript, sass and any gulp tasks

```bash
$ gulp
```

## Watch javascript, sass and any gulp tasks

```bash
$ gulp watch
```

## Add SVG to spritemap

Drop any svg into the `/src/svg` folder. The name of the svg will become the id of the svg. Make any changes such as `fill="currentColor"` to the svg inside of the `/src/svg` folder and Gulp will process.

```bash
$ gulp watch
```

## JavaScript
### File structure
* `/src/lib` -> `/js/lib` -- Any standalone JavaScript file. Usually for polyfills that only need to be loaded in IE.
* `/src/plugins` -> `/js/plugins.js` -- All files get concat, and minified into one plugins.js
* `/src/scripts.js` -> `/js/scripts.js` -- Minified