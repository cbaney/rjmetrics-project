$(document).ready(function(){
	// Add JS class to body
	$('body').addClass('js');

	// Show more product descriptions
	$('.btn-product-desc').on('click',function(){
		$this = $(this);

		// Toggle class to the additional product description to show or hide
		$this.next('.product-desc-more').toggleClass('show');

		// Toggle text to show more/less
		($this.text() === "Show More…") ? $this.text("Show Less…") : $this.text("Show More…");
	});
});